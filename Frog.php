<?php

require('animal.php');

class Frog extends Animal
{
    public $legs = 4;
    public $jump = "Hop Hop";
}