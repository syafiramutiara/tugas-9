<?php

// require('animal.php');
require('Frog.php');
require('Ape.php');

$sheep = new Animal("shaun");

echo "Name : $sheep->name <br>" ;
echo "legs : $sheep->legs  <br>" ;
echo "cold blooded : $sheep->cold_blooded <br> <br>";

$sheep2 = new frog ("buduk");

echo "Name : $sheep2->name <br>" ;
echo "legs : $sheep2->legs  <br>" ;
echo "cold blooded : $sheep2->cold_blooded <br>";
echo "Jump : $sheep2->jump <br> <br>";

$sheep2 = new Ape ("kera sakti");

echo "Name : $sheep2->name <br>" ;
echo "legs : $sheep2->legs  <br>" ;
echo "cold blooded : $sheep2->cold_blooded <br>";
echo "Yell : $sheep2->yell <br>";